package com.jfinal.plugin.activerecord.generator;

import com.jfinal.plugin.activerecord.generator.base.BaseBlog;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class Blog extends BaseBlog<Blog> {
	public static final Blog dao = new Blog().dao();
}
