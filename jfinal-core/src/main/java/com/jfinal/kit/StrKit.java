/**
 * Copyright (c) 2011-2019, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.kit;

import org.apache.commons.lang.text.StrBuilder;

import java.util.Random;

/**
 * StrKit.
 */
public class StrKit {

	/**
	 * 首字母变小写
	 */
	public static String firstCharToLowerCase(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'A' && firstChar <= 'Z') {
			char[] arr = str.toCharArray();
			arr[0] += ('a' - 'A');
			return new String(arr);
		}
		return str;
	}

	/**
	 * 首字母变大写
	 */
	public static String firstCharToUpperCase(String str) {
		char firstChar = str.charAt(0);
		if (firstChar >= 'a' && firstChar <= 'z') {
			char[] arr = str.toCharArray();
			arr[0] -= ('a' - 'A');
			return new String(arr);
		}
		return str;
	}

	/**
	 * 字符串为 null 或者内部字符全部为 ' ' '\t' '\n' '\r' 这四类字符时返回 true
	 */
	public static boolean isBlank(String str) {
		if (str == null) {
			return true;
		}
		int len = str.length();
		if (len == 0) {
			return true;
		}
		for (int i = 0; i < len; i++) {
			switch (str.charAt(i)) {
			case ' ':
			case '\t':
			case '\n':
			case '\r':
			// case '\b':
			// case '\f':
				break;
			default:
				return false;
			}
		}
		return true;
	}

	public static boolean notBlank(String str) {
		return !isBlank(str);
	}

	public static boolean notBlank(String... strings) {
		if (strings == null || strings.length == 0) {
			return false;
		}
		for (String str : strings) {
			if (isBlank(str)) {
				return false;
			}
		}
		return true;
	}

	public static boolean notNull(Object... paras) {
		if (paras == null) {
			return false;
		}
		for (Object obj : paras) {
			if (obj == null) {
				return false;
			}
		}
		return true;
	}

	public static String toCamelCase(String stringWithUnderline) {
		if (stringWithUnderline.indexOf('_') == -1) {
			return stringWithUnderline;
		}

		stringWithUnderline = stringWithUnderline.toLowerCase();
		char[] fromArray = stringWithUnderline.toCharArray();
		char[] toArray = new char[fromArray.length];
		int j = 0;
		for (int i=0; i<fromArray.length; i++) {
			if (fromArray[i] == '_') {
				// 当前字符为下划线时，将指针后移一位，将紧随下划线后面一个字符转成大写并存放
				i++;
				if (i < fromArray.length) {
					toArray[j++] = Character.toUpperCase(fromArray[i]);
				}
			}
			else {
				toArray[j++] = fromArray[i];
			}
		}
		return new String(toArray, 0, j);
	}

	public static String join(String[] stringArray) {
		StringBuilder sb = new StringBuilder();
		for (String s : stringArray) {
			sb.append(s);
		}
		return sb.toString();
	}

	public static String join(String[] stringArray, String separator) {
		StringBuilder sb = new StringBuilder();
		for (int i=0; i<stringArray.length; i++) {
			if (i > 0) {
				sb.append(separator);
			}
			sb.append(stringArray[i]);
		}
		return sb.toString();
	}

	public static boolean slowEquals(String a, String b) {
		byte[] aBytes = (a != null ? a.getBytes() : null);
		byte[] bBytes = (b != null ? b.getBytes() : null);
		return HashKit.slowEquals(aBytes, bBytes);
	}

	public static boolean equals(String a, String b) {
		return a == null ? b == null : a.equals(b);
	}

	public static String getRandomUUID() {
		return java.util.UUID.randomUUID().toString().replace("-", "");
	}


	//================================================================

	public static String replace(String text, String searchString, String replacement) {
		return replace(text, searchString, replacement, -1);
	}

	public static String replace(String text, String searchString, String replacement, int max) {
		if (!isBlank(text) && !isBlank(searchString) && replacement != null && max != 0) {
			int start = 0;
			int end = text.indexOf(searchString, start);
			if (end == -1) {
				return text;
			} else {
				int replLength = searchString.length();
				int increase = replacement.length() - replLength;
				increase = increase < 0 ? 0 : increase;
				increase *= max < 0 ? 16 : (max > 64 ? 64 : max);

				StrBuilder buf;
				for (buf = new StrBuilder(text.length() + increase); end != -1; end = text.indexOf(searchString, start)) {
					buf.append(text.substring(start, end)).append(replacement);
					start = end + replLength;
					--max;
					if (max == 0) {
						break;
					}
				}

				buf.append(text.substring(start));
				return buf.toString();
			}
		} else {
			return text;
		}
	}

	public static boolean contains(String ids, Long id) {
		if (StrKit.isBlank(ids)) {
			return false;
		}
		String dot = ",";
		return (dot + ids + dot).contains(dot + id + dot);
	}

	/**
	 * 将一个int数字转换为二进制的字符串形式。
	 *
	 * @param num    需要转换的int类型数据
	 * @param digits 要转换的二进制位数，位数不足则在前面补0
	 * @return 二进制的字符串形式
	 */
	public static String toBinary(int num, int digits) {
		String cover = Integer.toBinaryString(1 << digits).substring(1);
		String s = Integer.toBinaryString(num);
		return s.length() < digits ? cover.substring(s.length()) + s : s;
	}

	public static String toStr(Object obj) {
		if (obj == null) {
			return "";
		}
		return obj.toString();
	}


	public static String escapeSql(String str) {
		return str == null ? null : replace(str, "'", "''");
	}

	public static String strongEscapeSql(String str) {
		if (str == null) {
			return str;
		}
		str = escapeSql(str);
		str = replace(str, "\\", "");
		str = replace(str, "%", "");
		return str;
	}

	public static String randomNumber(int length) {
		String sources = "0123456789";
		Random rand = new Random();
		StringBuffer flag = new StringBuffer();
		for (int j = 0; j < length; j++) {
			flag.append(sources.charAt(rand.nextInt(9)) + "");
		}
		return flag.toString();
	}

	public static void main(String[] args) {
		// String code = StrKit.randomNumber(6);
		String code = StrKit.toBinary(2088892, 24);
		System.out.println(code);
	}
}




