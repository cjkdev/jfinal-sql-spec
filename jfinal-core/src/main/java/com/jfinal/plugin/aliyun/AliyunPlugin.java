package com.jfinal.plugin.aliyun;

import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.IPlugin;

/**
 * @author CHEN
 */
public abstract class AliyunPlugin implements IPlugin {

    protected Prop prop;

    protected String endpoint;
    protected String accessKey;
    protected String secretKey;


    public AliyunPlugin(String propFileName, String propPrefix) {
        this.prop = PropKit.append(propFileName);
        this.endpoint = prop.get(propPrefix + ".endpoint");
        this.accessKey = prop.get(propPrefix + ".accessKey");
        this.secretKey = prop.get(propPrefix + ".secretKey");
    }

}
