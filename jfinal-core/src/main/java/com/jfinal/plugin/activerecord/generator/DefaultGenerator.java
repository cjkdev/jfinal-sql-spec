package com.jfinal.plugin.activerecord.generator;

import com.alibaba.dcm.DnsCacheManipulator;
import com.jfinal.config.DruidPluginKit;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.dialect.MysqlDialect;
import com.jfinal.plugin.druid.DruidPlugin;

import javax.sql.DataSource;

/**
 * @author chen
 * @created 12/5/2016.
 */
public class DefaultGenerator extends Generator {


    public DefaultGenerator(DataSource dataSource, String baseModelPackageName, String baseModelOutputDir) {
        super(dataSource, baseModelPackageName, baseModelOutputDir);
    }

    public DefaultGenerator(DataSource dataSource, BaseModelGenerator baseModelGenerator) {
        super(dataSource, baseModelGenerator);
    }

    public DefaultGenerator(DataSource dataSource, BaseModelGenerator baseModelGenerator, ModelGenerator modelGenerator) {
        super(dataSource, baseModelGenerator, modelGenerator);
    }

    public DefaultGenerator(DataSource dataSource, String modelPackageName) {
        super(dataSource, modelPackageName, getModelOutputDir(modelPackageName));
    }

    private static String getModelOutputDir(String modelPackageName) {
        return PathKit.getWebRootPath() + "/src/main/java/" + modelPackageName.replace(".", "/");
    }

    @Override
    public void setMappingKitClassName(String mappingKitClassName) {
        String baseModelPackageName = super.modelGenerator.modelPackageName;
        String mappingKitOutputDir = getModelOutputDir(baseModelPackageName);
        MappingKitGenerator mappingKitGenerator = new MappingKitGenerator(baseModelPackageName, mappingKitOutputDir);
        mappingKitGenerator.setMappingKitClassName(mappingKitClassName);
        super.setMappingKitGenerator(mappingKitGenerator);
    }

    //=================================================================================

    public DefaultGenerator(String dbName, Class clazz) {
        this(dbName, clazz.getPackage().getName());
    }

    public DefaultGenerator(String dbName, String modelPackageName) {

        this(getDataSource(dbName), modelPackageName);
        String dataDictionaryFileName = "DataDictionary.txt";
        String mappingKitFileName = "MappingKit";

        if (StrKit.notBlank(dbName)) {
            dbName = dbName.toLowerCase();
            dataDictionaryFileName = dbName + "_" + dataDictionaryFileName;
            mappingKitFileName = dbName + "_" + mappingKitFileName;
        }

        super.setDataDictionaryFileName("_" + dataDictionaryFileName);
        super.setMappingKitClassName("_" + mappingKitFileName);

        // 设置数据库方言
        this.setDialect(new MysqlDialect());
        // 配置是否生成备注
        this.setGenerateRemarks(true);
        // 设置是否在 Model 中生成 dao 对象
        this.setGenerateDaoInModel(true);
        // 设置 BaseMode 是否生成链式 setter 方法
        this.setGenerateChainSetter(true);
        // 设置是否生成字典文件
        this.setGenerateDataDictionary(true);
    }

    public static DataSource getDataSource() {
        return getDataSource("");
    }

    public static DataSource getDataSource(String dbName) {
        PropKit.use("app.properties");
        DnsCacheManipulator.loadDnsCacheConfig();
        DruidPlugin druidPlugin = DruidPluginKit.create(dbName);
        druidPlugin.start();
        return druidPlugin.getDataSource();
    }

}
