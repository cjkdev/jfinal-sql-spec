package com.jfinal.plugin.activerecord.sql;

/**
 * Created by Chen on 15/5/26.
 */

public class SqlDelete extends SqlObject<SqlDelete> {

    public static final String DELETE = " DELETE ";

    public static final String FROM = " FROM ";

    public static void main(String[] args) {
        String sql = new SqlDelete().from("user").where("user=?", "aaa").toSql();
        System.out.println(sql);
    }

    public SqlDelete from(String tableName) {
        this.tableName = tableName;
        return this;
    }

    @Override
    public String toSql() {
        return DELETE + FROM + tableName + getSpecSql();
    }
}
