package com.jfinal.plugin.activerecord.sql;

/**
 * @author CHEN
 */
public abstract class SqlObject<M extends SqlObject> {

    protected Spec spec = new Spec();

    protected String tableName;

    public abstract String toSql();

    public Spec getSpec() {
        return this.spec;
    }


    public String getSpecSql() {
        return spec.toSql();
    }

    public String getExceptSql() {
        return spec.toExceptSql();
    }

    public String getWhereSql() {
        return spec.toWhereSql();
    }

    public M where(Cnd cnd) {
        this.spec = Spec.where(cnd);
        return (M) this;
    }

    public M where(Spec spec) {
        this.spec = spec;
        return (M) this;
    }

    public M where(String sqlWhere, Object... params) {
        this.spec = Spec.where(sqlWhere, params);
        return (M) this;
    }

    public SqlObject addGroupBy(String column) {
        this.spec.addGroupBy(column);
        return this;
    }

    public Object[] getParams() {
        return this.spec.getParams();
    }
}
