package com.jfinal.plugin.activerecord.sql;

/**
 * Created by luxx on 2016/7/12.
 */
public class Join {
    private String type;
    private String table;
    private String alias;
    private String conditions;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public Join(String type, String table, String alias, String conditions) {
        this.type = type;
        this.table = table;
        this.alias = alias;
        this.conditions = conditions;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }
        if (!(object instanceof Join)) {
            return false;
        }

        if (object == this) {
            return true;
        }
        if (getClass() != object.getClass()) {
            return false;
        }

        Join to = (Join) object;
        if (this.type == null || to.getType() == null || this.table == null || to.getTable() == null
                || this.alias == null || to.getAlias() == null
                || this.conditions == null || to.getConditions() == null) {
            return false;
        }
        if (this.type.equals(to.getType()) && this.table.equals(to.getTable())
                && this.alias.equals(to.getAlias()) && this.conditions.equals(to.getConditions())) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public String toString() {
        return type + " join " + table + " " + alias + " on " + conditions;
    }
}
