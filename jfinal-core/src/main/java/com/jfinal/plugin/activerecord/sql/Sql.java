package com.jfinal.plugin.activerecord.sql;

public class Sql {

    public static SqlSelect select() {
        return new SqlSelect();
    }

    public static SqlSelect select(String columns) {
        return select().columns(columns);
    }

    public static SqlDelete delete() {
        return new SqlDelete();
    }

    public static SqlUpdate update(String table) {
        return new SqlUpdate(table);
    }

    public static void main(String[] args) {
        String sql = Sql.select().from("user").where(Spec.where("age", ">", 1).limit(10).desc("age")).toSql();
        System.out.println(sql);

        String sql2 = Sql.select().from("user").where("age=?", 1).toSql();
        System.out.println(sql2);

        String sqlDelete = Sql.delete().from("user").where("a=?", 1).toSql();
        System.out.println(sqlDelete);

        String sqlUpdate = Sql.update("user").set("name",1).where("a=?", 1).toSql();
        System.out.println(sqlUpdate);
    }
}
