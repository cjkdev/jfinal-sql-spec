package com.jfinal.plugin.redisson;

import com.jfinal.kit.Prop;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.IPlugin;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.codec.JsonJacksonCodec;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;

/**
 * Redisson插件
 * <p>
 *
 * @author chen
 * @date 16/12/1
 */
public class RedissonPlugin implements IPlugin {

    private static final Log LOG = Log.getLog(RedissonPlugin.class);

    private RedissonClient redissonClient;

    private String redissonName = "main";

    private String host;
    private int port;
    private String password;
    private int database;
    private int connectionPoolSize;

    private Config config;

    public RedissonPlugin() {
        this("redis.properties");
    }

    public RedissonPlugin(String config) {
        this(new Prop(config));
    }

    public RedissonPlugin(Prop prop) {
        this.host = prop.get("redis.host");
        this.port = prop.getInt("redis.port");
        this.password = prop.get("redis.password");
        this.database = prop.getInt("redis.database", 0);
    }

    public RedissonPlugin(String host, int port) {
        this(host, port, null, 0);
    }

    public RedissonPlugin(String host, int port, int database) {
        this(host, port, null, database);
    }

    public RedissonPlugin(String host, int port, String password) {
        this(host, port, password, 0);
    }

    public RedissonPlugin(String host, int port, String password, int database) {
        this("main", host, port, password, database, 64);
    }

    public RedissonPlugin(String redissonName, String host, int port, String password, int database, int connectionPoolSize) {
        this.host = host;
        this.port = port;
        this.password = password;
        this.database = database;
        this.redissonName = redissonName;
        this.connectionPoolSize = connectionPoolSize;
    }

    public RedissonPlugin(Config config) {
        this.config = config;
    }

    @Override
    public boolean start() {

        LOG.info("init redisson start...");
        if (config == null) {
            config = new Config();
            //采用JsonJacksonCodec序列化
            config.setCodec(new JsonJacksonCodec());
            SingleServerConfig serverConfig = config.useSingleServer();
            serverConfig.setAddress("redis://" + host + ":" + port);
            serverConfig.setDatabase(database);
            if (!StrKit.isBlank(password)) {
                serverConfig.setPassword(password);
            }
            serverConfig.setConnectionPoolSize(connectionPoolSize);
        }
        this.redissonClient = Redisson.create(config);
        RedissonKit.addRedissonClient(redissonName, this.redissonClient);

        LOG.info("init redisson end.");

        return true;
    }

    @Override
    public boolean stop() {
        this.redissonClient.shutdown();
        return true;
    }


}
