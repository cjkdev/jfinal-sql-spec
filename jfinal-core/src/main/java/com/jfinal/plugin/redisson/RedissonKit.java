package com.jfinal.plugin.redisson;

import org.redisson.api.RedissonClient;

import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author chen
 * @date 16/12/1
 */
public class RedissonKit {


    static RedissonClient mainRedissonClient = null;

    private static final ConcurrentHashMap<String, RedissonClient> redissonClientMap = new ConcurrentHashMap<String, RedissonClient>();

    public static void addRedissonClient(String name, RedissonClient redissonClient) {
        if (redissonClient == null)
            throw new IllegalArgumentException("cache can not be null");
        if (redissonClientMap.containsKey(name))
            throw new IllegalArgumentException("The cache name already exists");

        redissonClientMap.put(name, redissonClient);
        if (mainRedissonClient == null)
            mainRedissonClient = redissonClient;
    }

    public static RedissonClient use() {
        return mainRedissonClient;
    }

    public static RedissonClient use(String name) {
        return redissonClientMap.get(name);
    }

}
