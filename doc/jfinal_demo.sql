/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50718
Source Host           : localhost:3306
Source Database       : jfinal_demo

Target Server Type    : MYSQL
Target Server Version : 50718
File Encoding         : 65001

Date: 2019-03-31 11:21:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for blog
-- ----------------------------
DROP TABLE IF EXISTS `blog`;
CREATE TABLE `blog` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `content` mediumtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of blog
-- ----------------------------
INSERT INTO `blog` VALUES ('1', 'JFinal Demo Title here', 'JFinal Demo Content here');
INSERT INTO `blog` VALUES ('2', 'test 1', 'test 1');
INSERT INTO `blog` VALUES ('3', 'test 2', 'test 2');
INSERT INTO `blog` VALUES ('4', 'test 3', 'test 3');
INSERT INTO `blog` VALUES ('5', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('6', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('7', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('8', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('9', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('10', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('11', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('12', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('13', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('14', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('15', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('16', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('17', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('18', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('19', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('20', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('21', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('22', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('23', 'test 4', 'test 4');
INSERT INTO `blog` VALUES ('24', 'test 4', 'test 4');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `blog_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'AAA', '1');
INSERT INTO `user` VALUES ('2', 'BBB', '1');
