# Jfinal-sql-spec 
##### 注意：为了命名统一，直接修改jfinal源代码添加功能，侵入性强，仅供参考。

#### 介绍
Jfinal-sql-spec 是对java极速web框架 jfinal spec查询的一个扩充，简化开发者的学习应用成本，为您节约更多时间，去陪恋人、家人和朋友。

##### SELECT * FROM `blog` WHERE title='test 1' 

```
   List<Blog> blogs = Blog.dao.findBy("title=?", "test 1");
    
```
##### SELECT * FROM `blog` WHERE title='test 1' OR title LIKE '%test%' GROUP BY title ORDER BY title DESC LIMIT 0, 100
```
        List<Blog> blogs = Blog.dao.findBy(
                Spec.where("title=?", "test 1")
                        .or("title", Cnd.__LIKE__, "test")
                        .orderBy("title", "desc")
                        .limit(100));
```

##### SELECT * FROM `blog` WHERE title='test 1' OR (title LIKE '%test') GROUP BY title ORDER BY title DESC LIMIT 0, 100

```
        List<Blog> blogs = Blog.dao.findBy(
                Spec.where("title=?", "test 1")
                        .or(Cnd.create("title", Cnd.__LIKE, "test"))
                        .groupBy("title")
                        .orderBy("title", "desc")
                        .limit(100));
```
#####  SELECT a.id,a.name,b.title FROM `user` a LEFT JOIN `blog` b ON a.blog_id=b.id WHERE a.id=1 LIMIT 0, 10

```
        SqlSelect sqlSelect =Sql.select("a.id,a.name,b.title")
                .from(User.table, "a")
                .leftJoin(Blog.table, "b", "a.blog_id=b.id")
                .where("a.id=?", 1);
        Page<Record> page = Db.paginate(sqlSelect);

```


#####   SELECT a.id,a.name,b.title FROM `user` a
#####   LEFT JOIN `blog` b ON a.blog_id=b.id
#####  WHERE a.name='AAA' OR (b.title LIKE '%test')
#####  ORDER BY title
#####  DESC LIMIT 0, 100    

```
        Spec spec = Spec.where("a.name=?", "AAA")
                .or(Cnd.create("b.title", Cnd.__LIKE, "test"))
                .orderBy("title", "desc")
                .limit(100);
        SqlSelect sqlSelect = Sql.select("a.id,a.name,b.title")
                .from(User.table, "a")
                .leftJoin(Blog.table, "b", "a.blog_id=b.id")
                .where(spec);
        Page<Record> page = Db.paginate(sqlSelect);
   
```