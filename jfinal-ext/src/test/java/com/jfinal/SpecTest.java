package com.jfinal;

import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.generator.Blog;
import com.jfinal.plugin.activerecord.generator.User;
import com.jfinal.plugin.activerecord.sql.Cnd;
import com.jfinal.plugin.activerecord.sql.Spec;
import com.jfinal.plugin.activerecord.sql.Sql;
import com.jfinal.plugin.activerecord.sql.SqlSelect;
import com.jfinal.test.BaseTestCase;
import org.junit.Test;

import java.util.List;

public class SpecTest extends BaseTestCase {

    @Test
    public void test_findBy() {
        List<Blog> blogs = Blog.dao.findBy("title=?", "test 1");
        System.out.println(blogs);
    }

    @Test
    public void test_sepc_1() {
        // SELECT * FROM `blog` WHERE title='test 1' OR title LIKE '%test%' GROUP BY title ORDER BY title DESC LIMIT 0, 100
        List<Blog> blogs = Blog.dao.findBy(
                Spec.where("title=?", "test 1")
                        .or("title", Cnd.__LIKE__, "test")
                        .orderBy("title", "desc")
                        .limit(100));
        System.out.println(blogs);
    }

    @Test
    public void test_sepc_2() {
        // SELECT * FROM `blog` WHERE title='test 1' OR (title LIKE '%test') GROUP BY title ORDER BY title DESC LIMIT 0, 100
        List<Blog> blogs = Blog.dao.findBy(
                Spec.where("title=?", "test 1")
                        .or(Cnd.create("title", Cnd.__LIKE, "test"))
                        .groupBy("title")
                        .orderBy("title", "desc")
                        .limit(100));
        System.out.println(blogs);
    }

    @Test
    public void test_sqlSelect() {
// SELECT a.id,a.name,b.title FROM `user` a LEFT JOIN `blog` b ON a.blog_id=b.id WHERE a.id=1 LIMIT 0, 10
        SqlSelect sqlSelect =Sql.select("a.id,a.name,b.title")
                .from(User.table, "a")
                .leftJoin(Blog.table, "b", "a.blog_id=b.id")
                .where("a.id=?", 1);

        Page<Record> page = Db.paginate(sqlSelect);
        System.out.println(page);
    }


    @Test
    public void test_sqlSelect_spec() {

//  SELECT a.id,a.name,b.title FROM `user` a
//  LEFT JOIN `blog` b ON a.blog_id=b.id
//  WHERE a.name='AAA' OR (b.title LIKE '%test')
//  ORDER BY title
//  DESC LIMIT 0, 100

        Spec spec = Spec.where("a.name=?", "AAA")
                .or(Cnd.create("b.title", Cnd.__LIKE, "test"))
                .orderBy("title", "desc")
                .limit(100);

        SqlSelect sqlSelect = Sql.select("a.id,a.name,b.title")
                .from(User.table, "a")
                .leftJoin(Blog.table, "b", "a.blog_id=b.id")
                .where(spec);

        Page<Record> page = Db.paginate(sqlSelect);
        System.out.println(page);
    }
}
