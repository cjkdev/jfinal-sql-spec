package com.jfinal.kit;

import com.jfinal.plugin.activerecord.Record;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class PoiKitTest {
    @Test
    public void test() {
        String file = "D:\\tmp\\APP_type_20181106.xlsx";
        String columArray[] = {"APPID", "APP名称", "开发者ID", "管理人", "APP类别"};
        String dbColumArary[] = {"appId", "appName", "developerId", "managerName", "appTag"};
        try {
            List<Record> records = PoiKit.getExcelRecord(file, columArray, dbColumArary);
            System.out.println(records.size());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        System.out.println(file);
    }
}
