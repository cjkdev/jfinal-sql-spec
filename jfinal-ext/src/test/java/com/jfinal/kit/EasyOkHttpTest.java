package com.jfinal.kit;

import com.mzlion.easyokhttp.HttpClient;
import com.mzlion.easyokhttp.response.callback.Callback;
import com.mzlion.easyokhttp.response.callback.CallbackAdaptor;
import com.mzlion.easyokhttp.response.handle.DataHandler;
import com.mzlion.easyokhttp.response.handle.StringDataHandler;
import okhttp3.Dispatcher;
import org.apache.commons.lang.time.StopWatch;
import org.junit.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

public class EasyOkHttpTest {

    @Test
    public void test() throws InterruptedException {

        AtomicInteger atomicInteger = new AtomicInteger();
        ExecutorService executorService = Executors.newFixedThreadPool(1000);

        for (int i = 0; i < 1000*1000; i++) {
            executorService.execute(()->{
                String url = "http://localhost:8080/count";
               // Dispatcher dispatcher = HttpClient.Instance.getOkHttpClient().dispatcher();
//                dispatcher.setMaxRequestsPerHost(100);
//                dispatcher.setMaxRequests(10000);

                 HttpClient.get(url).execute(new CallbackAdaptor<String>(){
                     @Override
                     public DataHandler<String> getDataHandler() {
                         return StringDataHandler.create();
                     }

                     @Override
                     public void onSuccess(String data) {
                         //data就是经过处理后的数据，直接在这里写自己的业务逻辑
                         System.out.println(atomicInteger.incrementAndGet());
                         System.out.println(data);
                     }
                 });
            });

        }

        Thread.sleep(10000000);
    }
}
