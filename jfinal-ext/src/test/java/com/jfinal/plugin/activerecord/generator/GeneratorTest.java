package com.jfinal.plugin.activerecord.generator;

import org.junit.Test;

/**
 * Created by CHEN on 2017/3/19.
 */
public class GeneratorTest {

    @Test
    public void test() {

        String packageName = GeneratorTest.class.getPackage().getName();
        DefaultGenerator generator = new DefaultGenerator("jfinal_demo", packageName);
        generator.addIncludedTable("blog","user");

        generator.setGenerateDataDictionary(false);

        generator.generate();

    }


}
