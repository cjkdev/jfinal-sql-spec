package com.jfinal.service;

import com.jfinal.kit.ReflectKit;
import com.jfinal.plugin.activerecord.Model;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Administrator on 2016/5/5.
 */
public class Service {

    private static Map<String, BaseService<?>> serviceMap = new ConcurrentHashMap<>();

    public static <M extends Model<?>> BaseService<M> get(Class<M> modelClass) {
        String className = modelClass.getName();
        BaseService<?> service = serviceMap.get(className);
        if (service == null) {
            String serviceName = className + "Service";
            serviceName = serviceName.replace("model", "service");
            service = ReflectKit.newInstance(serviceName);
            serviceMap.put(className, service);
        }
        return (BaseService<M>) service;
    }

}
