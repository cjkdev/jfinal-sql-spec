package com.jfinal.exception;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by CHEN on 16/4/30.
 */
public class BusinessException extends RuntimeException {
    private Map<String, Object> details = new HashMap<>();
    public BusinessException(String msg) {
        super(msg);
    }

    public BusinessException addDetail(String key, Object value) {
        this.addDetail(key, value);
        return this;
    }

    public BusinessException addDetail(Map<String, Object> detail) {
        this.details = detail;
        return this;
    }

    public Map<String, Object>  getDetails() {
        return this.details;
    }

    public String getDetailMessages() {
        if (details == null || details.isEmpty()) {
            return this.getMessage();
        }

        StringBuffer sb = new StringBuffer();
        sb.append(this.getMessage()).append("--");
        for (Map.Entry<String, Object> entry : details.entrySet()) {
            sb.append(entry.getValue()).append(",");
        }
        return sb.substring(0, sb.length() - 1);
    }

}