package com.jfinal.web.interceptor;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.Controller;

/**
 * WEB拦截器
 *
 * @author Chen
 */
public class WebInterceptor implements Interceptor {

    @Override
    public void intercept(Invocation ai) {
        Controller controller = ai.getController();
        controller.setAttr("__controllerKey__", ai.getControllerKey());
        ai.invoke();
    }

}
