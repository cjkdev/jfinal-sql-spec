package com.jfinal.web;

/**
 * 返回对象
 *
 * @author Chen
 */
public class Response {

    public static final String SUCCESS = "success";
    public static final String FAIL = "fail";

    private int code;
    private String message;
    private Object data;

    /**
     * 请求ID
     */
    private String reqId;

    public Response() {
    }

    public Response(String message) {
        this.message = message;
    }

    public Response(Integer code) {
        this.code = code;
    }

    public Response(int code, String message) {
        this(null, code, message);
    }

    public Response(String reqId, int code, String message) {
        this.reqId = reqId;
        this.code = code;
        this.message = message;
    }

    public static Response success() {
        return new Response(0, SUCCESS);
    }

    /**
     * @param message
     * @return
     */
    public static Response success(String message) {
        return new Response(0, message);
    }

    public static Response success(String reqId, String message) {
        return new Response(reqId, 0, message);
    }

    public static Response fail() {
        return new Response(-1, FAIL);
    }

    public static Response fail(String message) {
        return new Response(-1, message);
    }

    public static Response fail(String reqId, String message) {
        return new Response(reqId, -1, message);
    }


    public int getCode() {
        return code;
    }

    public Response setCode(int code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public Response setMessage(String message) {
        this.message = message;
        return this;
    }

    public Object getData() {
        return data;
    }

    public Response setData(Object data) {
        this.data = data;
        return this;
    }

    public boolean isSuccess() {
        return this.code == 0;
    }

    public String getReqId() {
        return reqId;
    }

    public Response setReqId(String reqId) {
        this.reqId = reqId;
        return this;
    }
}
