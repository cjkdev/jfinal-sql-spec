package com.jfinal.plugin.cache;

import com.jfinal.log.Log;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.BiConsumer;

/**
 * 缓存类
 * <p>
 *
 * @author chen
 * @date 17/1/5
 */
public class LocalCache<K, V> implements Reloadable {

    private static final Log LOG = Log.getLog(LocalCache.class);

    private Map<K, V> cache = new HashMap<>();

    private CacheLoader<K, V> cacheLoader;

    public LocalCache(CacheLoader<K, V> cacheLoader) {
        this.cacheLoader = cacheLoader;
    }

    /**
     * 交换缓存.
     */
    @Override
    public void reload() {
        try {
            Map<K, V> temp = load();
            if (temp != null) {
                cache = temp;
                this.cacheLoader.callback(cache);
            }
        } catch (Exception e) {
            LOG.warn("reload error: " + e.getMessage(), e);
        }
    }


    /**
     * 加载所有缓存.
     *
     * @return
     */
    private Map<K, V> load() {
        return this.cacheLoader.load();
    }

    public V get(K key) {
        return cache.get(key);
    }

    public V getOrDefault(K k, V defaultVal) {
        return cache.getOrDefault(k, defaultVal);
    }

    public Collection<V> getValues() {
        return cache.values();
    }

    public boolean containsKey(K key) {
        return cache.containsKey(key);
    }

    public boolean containsValue(V value) {
        return cache.containsValue(value);
    }

    public void forEach(BiConsumer<K, V> func) {
        cache.forEach(func);
    }

    public Set<Entry<K, V>> entrySet() {
        return cache.entrySet();
    }
}
