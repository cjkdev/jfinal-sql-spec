package com.jfinal.plugin.cache;

import java.util.Map;

/**
 * 缓存加载器
 * <p>
 *
 * @author chen
 * @date 17/1/5
 */
public interface CacheLoader<K, V> {

    /**
     * 加载所有缓存
     *
     * @return
     */
    Map<K, V> load();

    /**
     * 缓存加载成功后，回调方法
     *
     * @param cache
     */
    default void callback(Map<K, V> cache) {

    }

}
