package com.jfinal.plugin.cache;

/**
 * 可重新加载的接口
 * <p>
 * Created by chen on 17/3/17.
 */
public interface Reloadable {

    void reload();

}
