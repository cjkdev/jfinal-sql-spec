package com.jfinal.plugin.cache;

/**
 * 缓存构造器
 * <p>
 * Created by chen on 17/1/5.
 */
public class CacheBuilder<K, V> {

    public static CacheBuilder<Object, Object> newBuilder() {
        return new CacheBuilder<>();
    }

    public <K1 extends K, V1 extends V> LocalCache<K1, V1> build(CacheLoader<K1, V1> cacheLoader) {
        return new LocalCache<>(cacheLoader);
    }
}
