package com.jfinal.kit;

import org.apache.commons.lang.math.NumberUtils;

import java.math.BigDecimal;

/**
 * Created by luxx on 2016/7/7.
 */
public class NumKit {

    /**
     * 四舍五入，保留scale位小数
     *
     * @param number
     * @param scale
     * @return
     */
    public static Double roundHalfUp(Double number, int scale) {
        if (number == null) {
            return null;
        }
        return roundHalfUp(new BigDecimal(number), scale);
    }


    /**
     * 四舍五入，保留scale位小数
     *
     * @param number
     * @param scale
     * @return
     */
    public static Double roundHalfUp(BigDecimal number, int scale) {
        try {
            BigDecimal bigDecimal1 = number.setScale(scale, BigDecimal.ROUND_HALF_UP);
            return bigDecimal1.doubleValue();
        } catch (Exception e) {
            return null;
        }
    }

    public final static boolean isNumeric(String s) {
        return NumberUtils.isNumber(s);
    }

//    public final static Long toLong(String s) {
//        if (StrKit.isBlank(s) || !isNumeric(s)) {
//            return null;
//        }
//        try {
//            return Long.valueOf(s);
//        } catch (Exception e) {
//            return null;
//        }
//    }

    public final static Long toLong(Object o) {
        if (o == null) {
            return null;
        }
        if (StrKit.isBlank(o.toString()) || !isNumeric(o.toString())) {
            return null;
        }
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).longValue();
        }
        try {
            return Long.valueOf(o.toString());
        } catch (Exception e) {
            return null;
        }
    }

    public final static BigDecimal toBigDecimal(Object o) {
        if (o == null) {
            return null;
        }

        if (o instanceof BigDecimal) {
            return (BigDecimal) o;
        }

        if (o instanceof Double) {
            return new BigDecimal((Double) o);
        }

        if (o instanceof Float) {
            return new BigDecimal((Float) o);
        }

        if (o instanceof Long) {
            return new BigDecimal((Long) o);
        }

        if (o instanceof Integer) {
            return new BigDecimal((Integer) o);
        }
        if (!NumberUtils.isNumber(o.toString())) {
            return null;
        }
        try {
            return BigDecimal.valueOf(Double.valueOf(o.toString()));
        } catch (Exception e) {
            return null;
        }
    }


    /**
     * 转成int类型
     *
     * @param o
     * @param defaultValue
     * @return
     */
    public final static Integer toInt(Object o, Integer defaultValue) {
        if (o == null) {
            return defaultValue;
        }
        if (StrKit.isBlank(o.toString()) || !isNumeric(o.toString())) {
            return defaultValue;
        }
        if (o instanceof BigDecimal) {
            return ((BigDecimal) o).intValue();
        }
        return Integer.valueOf(o.toString());
    }

    /**
     * 转成int类型，默认为0
     *
     * @param o
     * @return
     */
    public final static Integer toInt(Object o) {
        return toInt(o, 0);
    }

    /**
     * 是否偶数
     *
     * @param num
     * @return
     */
    public static boolean isEven(int num) {
        return !isOdd(num);
    }


    /**
     * 是否偶数
     *
     * @param num
     * @return
     */
    public static boolean isOdd(int num) {
        if ((num & 1) != 1) {
            return false;
        }
        return true;
    }

    /**
     * 百分比
     *
     * @param num
     * @param total
     * @return
     */
    public static double percent(double num, double total) {
        return (num / total) * 100;
    }

    public static double percent(double num, double total, int scale) {
        return roundHalfUp(percent(num, total), scale);
    }
}