package com.jfinal.kit;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 异步执行工具类
 * <p>
 * Created by chen on 17/1/6.
 */
public class AsyncKit {

    private static final ExecutorService executorService = Executors.newFixedThreadPool(5);

    /**
     * 异步执行命令
     *
     * @param command
     */
    public static void execute(Runnable command) {
        executorService.execute(command);
    }
}
