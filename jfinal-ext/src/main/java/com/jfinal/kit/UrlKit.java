package com.jfinal.kit;


import com.anthonynsimon.url.URL;
import com.anthonynsimon.url.exceptions.MalformedURLException;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;

/**
 * Created by chen on 16/11/14.
 */
public class UrlKit {


    public static String encode(String data) {
        return encode(data, "utf-8");
    }

    public static String encode(String data, String enc) {
        if (data == null) {
            return "";
        }
        try {
            return URLEncoder.encode(data, enc);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static String decode(String data) {
        return decode(data, "utf-8");
    }

    public static String decode(String data, String enc) {
        if (data == null) {
            return "";
        }
        try {
            return URLDecoder.decode(data, enc);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException(e);
        }
    }

    public static Map<String, String> getQueryPairs(String url) {
        try {
            URL base = URL.parse(url);
            return base.getQueryPairs();
        } catch (MalformedURLException e) {
            throw new RuntimeException(e);
        }
    }

    public static String getUrlWithoutParam(String param) {
        return param.contains("?") ? param.substring(0, param.indexOf("?")) : param;
    }

    public static String getUrlParamsByMap(Map<String, String> map, boolean isSort) {
        if (map == null) {
            return "";
        } else {
            StringBuffer sb = new StringBuffer();
            ArrayList keys = new ArrayList(map.keySet());
            if (isSort) {
                Collections.sort(keys);
            }

            for (int s = 0; s < keys.size(); ++s) {
                String key = (String) keys.get(s);
                String value = map.get(key);
                sb.append(key + "=" + value);
                sb.append("&");
            }

            String var7 = sb.toString();
            if (var7.endsWith("&")) {
                var7 = var7.substring(0, var7.lastIndexOf("&"));
            }

            return var7;
        }
    }

    /**
     * 替换成https
     *
     * @param url
     * @return
     */
    public static String replaceToHttps(String url) {
        if (StrKit.isBlank(url)) {
            return url;
        }
        if (!url.startsWith("http")) {
            return "https://" + url;
        }
        return url.replace("http://", "https://");
    }

    /**
     * 替换成http
     *
     * @param url
     * @return
     */
    public static String replaceToHttp(String url) {
        if (StrKit.isBlank(url)) {
            return url;
        }
        if (!url.startsWith("http")) {
            return "http://" + url;
        }
        return url.replace("https://", "http://");
    }

    public static void main(String[] args) {
        String url = "http://itunes.apple.com/cn/app/id1072041116?mt=8";
        System.out.println(replaceToHttps(url));

        String url2 = "itunes.apple.com/cn/app/id1072041116?mt=8";
        System.out.println(replaceToHttps(url2));
        String url3 = "http://itracking.oway.mobi/tl?_id=45&aff_click_id=00138DB87D61E1483621504636548&source=xxx";
        System.out.println(getQueryPairs(url3));
    }


}