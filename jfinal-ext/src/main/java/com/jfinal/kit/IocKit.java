package com.jfinal.kit;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * IOC工具类
 * <p>
 * Created by chen on 17/3/18.
 */
public class IocKit {

    private static Map<String, Object> mapper = new ConcurrentHashMap<>();

    /**
     * 绑定接口与实现类
     *
     * @param clazz
     * @param object
     * @param <M>
     */
    public static <M> void bind(Class<M> clazz, M object) {
        mapper.put(clazz.getName(), object);
    }

    /**
     * 获取对应Model的dao实例
     *
     * @param clazz
     * @return
     */
    public static <M> M get(Class<M> clazz) {
        return (M) mapper.get(clazz.getName());
    }
}