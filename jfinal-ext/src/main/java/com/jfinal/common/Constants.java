package com.jfinal.common;

import com.jfinal.kit.PropKit;

/**
 * @author Chen on 15/3/27.
 */
public class Constants {

    public static final String SESSION_USER = "session_user";
    public static final String COOKIE_USER_LOGIN = "cookie_user_login";
    public static final String SESSION_CAPTCHA_MD5 = "session_captcha_md5";

    public static String getSessionLoginKey() {
        return PropKit.get("session.loginKey", SESSION_USER);
    }

    public static String getCookieSecretKey() {
        return PropKit.get("cookie.secretKey", COOKIE_USER_LOGIN);
    }

    public static String getCookieLoginKey() {
        return PropKit.get("cookie.loginKey", COOKIE_USER_LOGIN);
    }

    public static String getSysName() {
        return PropKit.get("sys.name");
    }

    public static String getSysDomain() {
        return PropKit.get("sys.domain");
    }

}
