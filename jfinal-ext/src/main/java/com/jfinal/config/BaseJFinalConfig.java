package com.jfinal.config;

import com.alibaba.dcm.DnsCacheManipulator;
import com.jfinal.core.JFinal;
import com.jfinal.ext.handler.ContextPathHandler;
import com.jfinal.json.FastJsonFactory;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Log;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.template.Engine;
import com.jfinal.web.handler.JSessionIdHandler;
import com.jfinal.web.interceptor.ExceptionInterceptor;

import java.io.File;


/**
 * @author CHEN
 */
public abstract class BaseJFinalConfig extends AutoJFinalConfig {

    private static final Log LOG = Log.getLog(BaseJFinalConfig.class);

    public static void main(String[] args) {
        JFinal.start("src/main/webapp", 8080, "/", 10);
    }

    @Override
    public void configConstant(Constants me) {
        PropKit.use("app.properties");
        configWebConstant(me);
        configDnsCache();
        configIocBind();
    }


    protected void configWebConstant(Constants me) {
        me.setDevMode(PropKit.getBoolean("devMode", false));
        me.setError401View("/auth/login.html");
        me.setError403View("/auth/login.html");
        me.setError404View("/common/error/404.html");
        me.setError500View("/common/error/500.html");
        me.setBaseUploadPath(PathKit.getWebRootPath() + File.separator + "upload");
        me.setJsonFactory(new FastJsonFactory());
    }

    protected void configDnsCache() {
        try {
            DnsCacheManipulator.loadDnsCacheConfig();
        } catch (Exception e) {
            LOG.warn("config dns cache fail: " + e.getMessage());
        }
    }

    /**
     * 配置IOC绑定
     */
    protected void configIocBind() {
    }


    @Override
    public void configRoute(Routes me) {
    }

    @Override
    public void configEngine(Engine me) {
        me.addSharedMethod(new StrKit());
    }

    /**
     * 拦截器配置
     */
    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new ExceptionInterceptor());
    }

    /**
     * handler配置
     */
    @Override
    public void configHandler(Handlers me) {
        me.add(new ContextPathHandler("ctxPath"));
        me.add(new JSessionIdHandler());
    }
}
