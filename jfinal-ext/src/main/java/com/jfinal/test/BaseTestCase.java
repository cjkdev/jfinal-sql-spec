package com.jfinal.test;

import com.alibaba.dcm.DnsCacheManipulator;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.kit.PropKit;
import com.jfinal.config.BaseJFinalConfig;
import org.junit.After;
import org.junit.Before;

/**
 * 测试用例-基类
 *
 * @author Chen on 15/4/23.
 */
public abstract class BaseTestCase {

    private static final Plugins plugins = new Plugins();

    @Before
    public void setUpBeforeClass() {
        PropKit.use("app.properties");
        DnsCacheManipulator.loadDnsCacheConfig();
        createJFinalConfig().configPlugin(plugins);
        plugins.getPluginList().forEach(p -> p.start());
    }

    protected JFinalConfig createJFinalConfig() {
        return new BaseJFinalConfig() {
            @Override
            public void configPlugin(Plugins me) {
                configActiveRecordPlugin(me);
            }
        };
    }

    @After
    public void tearDown() {
        plugins.getPluginList().forEach(p -> p.stop());
    }

}